import { readFileSync } from 'fs'

export const connectorTypes = ['STRAVA', 'SPOTIFY', 'LINKEDIN']

export const rawData = JSON.parse(readFileSync('./raw_data_spotify.json', { encoding: 'utf-8' }))

export const jsonSchema = JSON.parse(readFileSync('./json_schema_spotify.json', { encoding: 'utf-8' }))
