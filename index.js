import { config } from 'dotenv'
import { encode } from 'gpt-3-encoder'
import { Configuration, OpenAIApi } from 'openai'
import { connectorTypes, jsonSchema, rawData } from './inputs.js'
import { getPropertyPaths, extractFields } from './utils.js'

config()

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
})

const openai = new OpenAIApi(configuration)

async function tryToComplete(prompt, transform = v => v, attempts = 5) {
  const response = await openai.createCompletion({
    model: 'text-davinci-003',
    prompt,
    max_tokens: 4000 - encode(prompt).length,
  })

  const result = response.data.choices[0].text?.trim()
  if (result) {
    try {
      const transformed = await transform(result)
      if (transformed) return transformed
    } catch {}
  }

  if (attempts > 0) {
    return tryToComplete(prompt, transform, attempts - 1)
  }

  return undefined
}

async function getNecessaryConnectorType(question, connectorTypes) {
  const prompt = `\
You are a helpful AI assistant.

Respond with a service name that might have data to answer the question.
Possible answers: ${[connectorTypes, 'UNSUPPORTED'].join(', ')}.

Q: Give me band recommendations.
A: SPOTIFY

Q: How many miles have I walked since Tuesday?
A: STRAVA

Q: What's the name of the company I worked on in 2017?
A: LINKEDIN

Q: How many photos do I have on my cloud?
A: UNSUPPORTED

Q: ${question}
A: `

  return tryToComplete(prompt, (response) => {
    return connectorTypes.find(connectorType => connectorType.toLowerCase() === response.toLowerCase())
  })
}

async function getNecessaryFields(question, jsonSchema, connectorType) {
  const paths = getPropertyPaths(jsonSchema)

  const prompt = `\
You are a helpful AI assistant.

Given an array of JSON fields of ${connectorType} data:
${JSON.stringify(paths)}

Respond with a JSON array of fields which might potentially answer the question.
Keep the list short.

Q: ${question}
A:`

  return tryToComplete(prompt, (response) => {
    const fields = JSON.parse(response)
    if (Array.isArray(fields) && fields.every(field => paths.includes(field))) {
      return fields
    }
  })
}

async function explainData(data, connectorType) {
  const prompt = `\
You are a helpful AI assistant.

Transform this ${connectorType} JSON data of a user into a human-readable text:
${JSON.stringify(data)}

Include all of the data and use human-readable names for properties.
Format dates properly and don't exclude time.

The description:`

  return tryToComplete(prompt)
}

async function answerQuestion(question, dataExplanation, connectorType) {
  const prompt = `\
You are a helpful AI assistant.
Current date is ${new Date().toDateString()}, ${new Date().toTimeString()}.

Given this ${connectorType} data:
${dataExplanation}

Answer the question. Explain your reasoning step by step, but keep the answer short.
Try avoiding complex calculations and keep the answer simple.

Q: ${question}
A:`

  return tryToComplete(prompt)
}

async function run(question) {
  console.log('\nQ:', question)
  const connectorType = await getNecessaryConnectorType(question, connectorTypes)
  // console.log({ connectorType })

  if (connectorType) {
    const fields = await getNecessaryFields(question, jsonSchema, connectorType)
    // console.log({ fields })

    if (fields) {
      const extractedData = extractFields(rawData, fields)
      const dataExplanation = await explainData(extractedData, connectorType)
      // console.log({ dataExplanation })

      if (dataExplanation) {
        return answerQuestion(question, dataExplanation, connectorType)
      } else {
        return 'I do not know the answer :('
      }
    } else {
      return `Not sure what ${connectorType} data should I use to answer the question :(`
    }
  } else {
    return 'Not sure which connector has data to answer this question :('
  }
}

// console.log('A:', await run('How often do I run in the mornings?'))
// console.log('A:', await run('What is my favorite type of exercise?'))
// console.log('A:', await run('Did I run in Ukraine?'))
// console.log('A:', await run('Do I walk more than I run?'))
// console.log('A:', await run('How many kilometers have I walked and ran in total?'))
// console.log('A:', await run('Am I exercising enough to be healthy?'))
// console.log('A:', await run('Should I run more?'))
// console.log('A:', await run('How often do I walk?'))
// console.log('A:', await run('Based on my usual sport activities, what should I work on?'))

console.log('A:', await run('What are my favorite artists and bands?'))
console.log('A:', await run('Which music genres I usually listen to?'))
console.log('A:', await run('Are there any anomalies in my music taste?'))
console.log('A:', await run('Recommend me some new bands to listen to.'))
console.log('A:', await run('Which music genre should I try out?'))
console.log('A:', await run('Am I a fan of a particular artist?'))

