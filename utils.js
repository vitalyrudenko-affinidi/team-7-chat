export function getPropertyPaths(schema, path = '') {
  const propertyPaths = [];
  
  if (schema.type === 'object') {
    const properties = schema.properties;
    for (const propertyName in properties) {
      const propertySchema = properties[propertyName];
      const propertyPath = `${path}.${propertyName}`.replace(/^\./, '');
      const nestedPropertyPaths = getPropertyPaths(propertySchema, propertyPath);
      propertyPaths.push(...nestedPropertyPaths);
    }
  } else if (schema.type === 'array' && schema.items) {
    const itemsSchema = schema.items;
    const itemsPath = `${path}[]`.replace(/^\./, '');
    const nestedPropertyPaths = getPropertyPaths(itemsSchema, itemsPath);
    propertyPaths.push(...nestedPropertyPaths);
  } else {
    const propertyPath = `${path}`.replace(/^\./, '');
    propertyPaths.push(propertyPath);
  }
  
  return propertyPaths;
}

export function extractFields(input, fields, previousKey = '') {
  if (Array.isArray(input)) {
    return input.map(item => extractFields(item, fields, previousKey ? `${previousKey}[]` : '[]'))
  }

  if (input && typeof input === 'object') {
    const result = {}
  
    for (const key of Object.keys(input)) {
      const completeKey = previousKey ? `${previousKey}.${key}` : key

      if (fields.some(field => field === completeKey)) {
        result[key] = input[key]
      } else if (fields.some(field => field.startsWith(completeKey))) {
        result[key] = extractFields(input[key], fields, completeKey)
      }
    }

    return result;
  }

  return input
}
